<?php

namespace Drupal\PIFRConduit;

class Converter
{
  protected $offset;
  protected $info = array();
  protected $map = array();

  public function __construct(array $offset = array())
  {
    // Defaults applicable to drupal.org based on around 1.6 million nodes and
    // 4.5 million files currently with room for expansion.
    $this->offset = $offset + array(
      'project' => 0,
      'branch' => 0,
      'file' => pow(10, 7) * 2, // 20 million.
      'factor' => 5,
    );
  }

  public function convert(array $batch)
  {
    $types = array(
      'projects' => 'project',
      'branches' => 'branch',
      'files' => 'file',
    );
    $nodes = array();
    foreach ($types as $key => $type) {
      $func = 'convert' . ucfirst($type);
      foreach ($batch[$key] as $item) {
        $nodes[$item['client_identifier']] = $this->$func($item);
      }
    }

    foreach ($batch['branches'] as &$branch) {
      $nodes[$branch['client_identifier']] = $this->addDependencies($nodes, $branch, $nodes[$branch['client_identifier']]);
    }
    return $nodes;
  }

  protected function convertProject(array $project)
  {
    $url = $project['repository_type'] . '://' . $project['repository_url'];
    $info = $this->getInfo($url);
    $this->info[$project['client_identifier']] = $info;

    return array(
      'type' => 'conduit_group',
      'nid' => $this->getOffset('project', $project['client_identifier']),
      'title' => $project['name'],
      'properties' => $this->field(array(
        'path' => array($info['path']),
      )),
    );
  }

  protected function convertBranch(array $branch)
  {
    // TODO Dependency, test.
    $info = $this->info[$branch['project_identifier']];
    $this->map[$branch['client_identifier']] = $branch['project_identifier'];
    return array(
      'type' => 'conduit_group',
      'nid' => $this->getOffset('branch', $branch['client_identifier']),
      'title' => $branch['vcs_identifier'],
      'conduit_parent' => $this->field($branch['project_identifier'] + $this->offset['project'], 'target_id'),
      'properties' => $this->field(array(
        'vcs' => array(
          $info['path'] => $info['url'] . '/' . $branch['vcs_identifier'],
        ),
        'core' => $branch['plugin_argument']['drupal.core.version'],
      ) + (
        $branch['plugin_argument']['drupal.core.version'] == 8 ?
        array('nomask' => '/(vendor)|((\\.\\.?|CVS|.*\\.api\\.php)$)/') :
        array()
      )),
    );
  }

  protected function convertFile(array $file)
  {
    $info = $this->info[$this->map[$file['branch_identifier']]];
    return array(
      'type' => 'conduit_group',
      'nid' => $this->getOffset('file', $file['client_identifier']),
      'title' => basename($file['file_url']),
      'conduit_parent' => $this->field($file['branch_identifier'] + $this->offset['branch'], 'target_id'),
      'properties' => $this->field(array(
        'patch' => array(
          $info['path'] => $file['file_url'],
        ),
      )),
    );
  }

  protected function addDependencies(array $nodes, array $branch, $self)
  {
    if ($branch['dependency']) {
      $dependencies = explode(',', $branch['dependency']);
      foreach ($dependencies as $dependency) {
        if (isset($nodes[$dependency])) {
          $dep_branch = $nodes[$dependency];
          $properties = $dep_branch['properties']['und'][0]['value'];
          $project_info = $this->info[$dep_branch['conduit_parent']['und'][0]['target_id']];
          $self['properties']['und'][0]['value']['vcs'][$project_info['path']] = $project_info['url'] . '/' . basename($properties['vcs'][$project_info['path']]);
        }
        else {
          // TODO watchdog
        }
      }
    }
    return $self;
  }

  protected function field($value, $key = 'value')
  {
    return array(
      'und' => array(
        0 => array(
          $key => $value,
        ),
      )
    );
  }

  protected function getInfo($url)
  {
    $info = array();
    $info['name'] = basename($url, '.git');
    $info['core'] = $info['name'] == 'drupal';
    $info['path'] = $info['core'] ? '' : 'sites/all/modules/' . $info['name'];
    $info['url'] = $url;
    return $info;
  }

  protected function getOffset($type, $id)
  {
    return $this->offset[$type] + ($id * $this->offset['factor']);
  }
}
