<?php

require 'vendor/autoload.php';

use Drupal\PIFRConduit\Converter;

$batch = array
(
  'branches' => array
  (
    array
    (
      'project_identifier' => 3060,
      'client_identifier' => 572834,
      'vcs_identifier' => '8.x',
      'dependency' => '',
      'plugin_argument' => array
      (
        'drupal.core.version' => 8
      ),
      'test' => FALSE,
      'link' => 'http://drupal.org/node/572834',
    ),
    array
    (
      'project_identifier' => 3061,
      'client_identifier' => 572835,
      'vcs_identifier' => '7.x-1.x',
      'dependency' => '',
      'plugin_argument' => array
      (
        'drupal.core.version' => 7
      ),
      'test' => TRUE,
      'link' => 'http://drupal.org/node/572835',
    ),
    array
    (
      'project_identifier' => 3062,
      'client_identifier' => 572836,
      'vcs_identifier' => '7.x-1.x',
      'dependency' => '572835',
      'plugin_argument' => array
      (
        'drupal.core.version' => 7
      ),
      'test' => TRUE,
      'link' => 'http://drupal.org/node/572836',
    ),
  ),
  'files' => array
  (
    array
    (
      'client_identifier' => 4313106,
      'file_url' => 'http://drupal.org/files/1608842_170.patch',
      'branch_identifier' => 572834,
      'link' => 'http://drupal.org/node/1608842#comment-6536918',
    ),
  ),
  'projects' => array
  (
    array
    (
      'client_identifier' => 3060,
      'name' => 'Drupal core',
      'repository_type' => 'git',
      'repository_url' => 'git://git.drupal.org/project/drupal.git',
      'link' => 'http://drupal.org/project/drupal',
    ),
    array
    (
      'client_identifier' => 3061,
      'name' => 'Other 1',
      'repository_type' => 'git',
      'repository_url' => 'git://git.drupal.org/project/other1.git',
      'link' => 'http://drupal.org/project/other1',
    ),
    array
    (
      'client_identifier' => 3062,
      'name' => 'Other 2',
      'repository_type' => 'git',
      'repository_url' => 'git://git.drupal.org/project/other2.git',
      'link' => 'http://drupal.org/project/other2',
    ),
  ),
);

$correct = array (
  3060 => 
  array (
    'nid' => 15300,
    'title' => 'Drupal core',
    'properties' => 
    array (
      'und' => 
      array (
        0 => 
        array (
          'value' => 
          array (
            'path' => 
            array (
              0 => '',
            ),
          ),
        ),
      ),
    ),
  ),
  3061 => 
  array (
    'nid' => 15305,
    'title' => 'Other 1',
    'properties' => 
    array (
      'und' => 
      array (
        0 => 
        array (
          'value' => 
          array (
            'path' => 
            array (
              0 => 'sites/all/modules/other1',
            ),
          ),
        ),
      ),
    ),
  ),
  3062 => 
  array (
    'nid' => 15310,
    'title' => 'Other 2',
    'properties' => 
    array (
      'und' => 
      array (
        0 => 
        array (
          'value' => 
          array (
            'path' => 
            array (
              0 => 'sites/all/modules/other2',
            ),
          ),
        ),
      ),
    ),
  ),
  572834 => 
  array (
    'nid' => 2864170,
    'title' => '8.x',
    'conduit_parent' => 
    array (
      'und' => 
      array (
        0 => 
        array (
          'target_id' => 3060,
        ),
      ),
    ),
    'properties' => 
    array (
      'und' => 
      array (
        0 => 
        array (
          'value' => 
          array (
            'vcs' => 
            array (
              '' => 'git://git://git.drupal.org/project/drupal.git/8.x',
            ),
            'core' => 8,
            'nomask' => '/(vendor)|((\\.\\.?|CVS|.*\\.api\\.php)$)/',
          ),
        ),
      ),
    ),
  ),
  572835 => 
  array (
    'nid' => 2864175,
    'title' => '7.x-1.x',
    'conduit_parent' => 
    array (
      'und' => 
      array (
        0 => 
        array (
          'target_id' => 3061,
        ),
      ),
    ),
    'properties' => 
    array (
      'und' => 
      array (
        0 => 
        array (
          'value' => 
          array (
            'vcs' => 
            array (
              'sites/all/modules/other1' => 'git://git://git.drupal.org/project/other1.git/7.x-1.x',
            ),
            'core' => 7,
          ),
        ),
      ),
    ),
  ),
  572836 => 
  array (
    'nid' => 2864180,
    'title' => '7.x-1.x',
    'conduit_parent' => 
    array (
      'und' => 
      array (
        0 => 
        array (
          'target_id' => 3062,
        ),
      ),
    ),
    'properties' => 
    array (
      'und' => 
      array (
        0 => 
        array (
          'value' => 
          array (
            'vcs' => 
            array (
              'sites/all/modules/other2' => 'git://git://git.drupal.org/project/other2.git/7.x-1.x',
              'sites/all/modules/other1' => 'git://git://git.drupal.org/project/other1.git/7.x-1.x',
            ),
            'core' => 7,
          ),
        ),
      ),
    ),
  ),
  4313106 => 
  array (
    'nid' => 41565530,
    'title' => '1608842_170.patch',
    'conduit_parent' => 
    array (
      'und' => 
      array (
        0 => 
        array (
          'target_id' => 572834,
        ),
      ),
    ),
    'properties' => 
    array (
      'und' => 
      array (
        0 => 
        array (
          'value' => 
          array (
            'patch' => 
            array (
              '' => 'http://drupal.org/files/1608842_170.patch',
            ),
          ),
        ),
      ),
    ),
  ),
);

$converter = new Converter();
$result = $converter->convert($batch);

var_dump($result == $correct);
